using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public Text timerText;
    public string text;
    private float time;
    public bool hasStopped;

    // Start is called before the first frame update
    void Start()
    {
        timerText.text = text;
        time = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (hasStopped != true)
        {
            float f = Time.time - time;
            string minutes = ((int)f / 60).ToString();
            string seconds = (f % 60).ToString("F0");
            timerText.text = text + ": " + minutes + ":" + seconds;

        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            hasStopped = true;
        }
    }
}
